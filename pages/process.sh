#!/bin/bash


#mkdir -p output
#for f in $(ls sample_xml); do
#    b=$(basename $f '.xml')
#    echo "Processing: $f"
#    ./transform -d sample_xml/$f -t cb-transforms/page.xsl > output/$b.html
#done


tranform() {
    FILE="$1"
    TRANSFORM="cb-transforms/page.xsl"

    echo $FILE
}
export -f transform

IN='data/xml'
OUT='new-data'

i=0
mkdir -p $OUT
find $IN -type f | while read file ; do
    echo $file
    FILENAME=${file#$IN}
    PATHNAME=$(dirname $FILENAME)
    FILENAME=$(basename $FILENAME '.xml')
    echo $IN $PATHNAME $FILENAME

    # create the output hierarchy
    mkdir -p $OUT/$PATHNAME
    ./transform -d $file -t cb-transforms/page.xsl > $OUT/$PATHNAME/"$FILENAME.html"
    echo
    i=$((i + 1))

    [[ $i == "5" ]] && exit
done