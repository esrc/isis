<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:import href="footer.xsl"  />
    <xsl:import href="sidebar.xsl" />
    <xsl:import href="mobile-header.xsl" />
    <xsl:import href="content.xsl" />

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes" />
    <xsl:template match="/">

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Marco La Rosa" />
    <link rel="icon" href="../../favicon.ico" />

    <title>Isis Current Bibliography</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css" />

    <!-- Custom styles for this template -->
    <link href="assets/css/site.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container-fluid" style="max-width: 1200px;">
      <div class="row mobile-header">
        <div class="col-xs-12">
          <xsl:call-template name="mobile-header" />
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-3 sidebar">
          <xsl:call-template name="sidebar" />
        </div>
        <div class="col-xs-12 col-sm-8 col-sm-offset-4 col-md-9 col-md-offset-3 col-lg-9 col-lg-offset-3 colmain">
            <xsl:call-template name="content" />
            <xsl:call-template name="footer" />
            <!-- <div id="footer"></div> -->
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Latest compiled and minified JavaScript -->
    <script src="http://code.jquery.com/jquery-2.1.3.min.js">//</script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js">//</script>
    <!-- <script src="assets/js/site.js">//</script> -->

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>-->
  </body>
</html>

    </xsl:template>
</xsl:stylesheet>