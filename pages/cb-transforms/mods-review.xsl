<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:eac="http://eac.staatsbibliotek-berlin.de/schema/cpf.xsd"
    xmlns:eac-cpf="urn:isbn:1-931666-33-4"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    exclude-result-prefixes="mods eac eac-cpf xlink str"
    version="1.0">

    <xsl:import href="common.xsl" />

    <xsl:template name="process-review">
        <!-- record header -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <span style="background-color: #BAD0EF; padding: 15px; display: inline-block; width: 100%; border-radius: 8px;">
                    REVIEW
                </span>
                <h3><xsl:value-of select="/mods:mods/mods:titleInfo/mods:title" /></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-3 col-lg-3">
                <strong>Publication Date</strong>
            </div>
            <div class="col-sm-8 col-md-9 col-lg-9">
                <xsl:value-of select="/mods:mods/mods:originInfo/mods:dateCreated" />
            </div>
        </div>

        <xsl:if test="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='author'] != ''">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-lg-3">
                    <strong>Author(s)</strong> 
                </div>
                <div class="col-sm-8 col-md-9 col-lg-9">
                    <xsl:apply-templates select="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='author']" />
                </div>
            </div>
        </xsl:if>
        <xsl:if test="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='editor'] != ''">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-lg-3">
                    <strong>Editor(s)</strong>
                </div>
                <div class="col-sm-8 col-md-9 col-lg-9">
                    <xsl:apply-templates select="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='editor']" />
                </div>
            </div>
        </xsl:if>
        <div class="row">
            <div class="col-sm-4 col-md-3 col-lg-3">
               <strong>Publication Information</strong>
            </div>
            <div class="col-sm-8 col-md-9 col-lg-9">
                <xsl:if test="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='editor'] = ''">---</xsl:if>
                <xsl:apply-templates select="/mods:mods/mods:originInfo/mods:place/mods:placeTerm" />
                <xsl:text>; </xsl:text>
                <xsl:apply-templates select="/mods:mods/mods:originInfo/mods:publisher" />
            </div>
        </div>
        <xsl:if test="/mods:mods/mods:language/mods:languageTerm != ''">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-lg-3">
                    <strong>Language</strong>
                </div>
                <div class="col-sm-8 col-md-9 col-lg-9">
                    <xsl:value-of select="/mods:mods/mods:language/mods:languageTerm" />
                </div>
            </div>
        </xsl:if>
        <xsl:if test="/mods:mods/mods:physicalDescription/mods:note != ''">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-lg-3">
                    Physical Description
                </div>
                <div class="col-sm-8 col-md-9 col-lg-9">
                    <xsl:apply-templates select="/mods:mods/mods:physicalDescription/mods:note" />
                </div>
            </div>
        </xsl:if>
        <xsl:if test="/mods:mods/mods:identifier[@type='doi'] != ''">
            <div class="row">
                <div class="col-sm-4 col-md-3 col-lg-3">
                    DOI
                </div>
                <div class="col-sm-8 col-md-9 col-lg-9">
                    <xsl:variable name="link" select="/mods:mods/mods:identifier[@type='doi']" />
                    <a href="http://doi.org/{$link}">
                        <xsl:value-of select="/mods:mods/mods:identifier[@type='doi']" />
                    </a>
                </div>
            </div>
        </xsl:if>
        <br/>

        <!-- conditional relationships for small media -->
        <div class="row mobile-header">
            <div class="col-sm-12 col-md-12 col-lg-12" style="background-color: #f5f5f5; padding: 15px; border-radius: 8px;">
                <h4>Links</h4>
                <xsl:call-template name="mods-relationships" />
            </div>
        </div>
        <!-- REVIEWS -->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <xsl:call-template name="review-of" />
            </div>
        </div>

    </xsl:template>
</xsl:stylesheet>
