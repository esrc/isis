<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:eac="http://eac.staatsbibliotek-berlin.de/schema/cpf.xsd"
    xmlns:eac-cpf="urn:isbn:1-931666-33-4"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    exclude-result-prefixes="mods eac eac-cpf xlink str"
    version="1.0">

    <xsl:template name="record-info">
        <xsl:variable name="recordInfo" select="/mods:mods/mods:recordInfo/mods:recordOrigin" />
        <p>
            <xsl:value-of select="/mods:mods/mods:recordInfo/mods:recordOrigin" />
        </p>
    </xsl:template>

    <xsl:template name="resources-about">
        <hr/>
        <h4>Resources About</h4>

        <xsl:variable name="limit" select="'5'" />
        <xsl:variable name="subjectOf" select="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:relations/eac-cpf:resourceRelation[@resourceRelationType='subjectOf'][position() &lt; $limit]" />
        <xsl:if test="$subjectOf != ''">
            <ul>
                <xsl:apply-templates select="$subjectOf" />
            </ul>
        </xsl:if>

        <xsl:variable name="countSubjectOf" select="count(//eac-cpf:resourceRelation[@resourceRelationType='subjectOf'])" />
        <xsl:if test="$countSubjectOf &gt; $limit">
            <div class="alert alert-info text-center">Show additional resources.</div>
        </xsl:if>
        <xsl:if test="$countSubjectOf = 0">
            <em>There are no resources in this category.</em>
        </xsl:if>

        <!-- -->
    </xsl:template>
    <xsl:template name="resources-by">
        <hr/>
        <h4>Resources By</h4>

        <xsl:variable name="limit" select="'5'" />
        <xsl:variable name="creatorOf" select="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:relations/eac-cpf:resourceRelation[@resourceRelationType='creatorOf'][position() &lt; $limit]" />
        <xsl:if test="$creatorOf != ''">
            <ul>
                <xsl:apply-templates select="$creatorOf" />
            </ul>
        </xsl:if>

        <xsl:variable name="countCreatorOf" select="count(//eac-cpf:resourceRelation[@resourceRelationType='creatorOf'])" />
        <xsl:if test="$countCreatorOf &gt; $limit">
            <div class="alert alert-info text-center">Show additional resources.</div>
        </xsl:if>
        <xsl:if test="$countCreatorOf = 0">
            <em>There are no resources in this category.</em>
        </xsl:if>
    </xsl:template>
    <xsl:template match="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:relations/eac-cpf:resourceRelation[@resourceRelationType='subjectOf']">
        <li>
            <a href="@xlink:href"><xsl:value-of select="eac-cpf:relationEntry" /></a><br/>
        </li>
    </xsl:template>

    <xsl:template match="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:relations/eac-cpf:resourceRelation[@resourceRelationType='creatorOf']">
        <li>
            <a href="@xlink:href"><xsl:value-of select="eac-cpf:relationEntry" /></a><br/>
        </li>
    </xsl:template>



    <xsl:template name="mods-relationships">
        <xsl:if test="/mods:mods/mods:genre = 'Review'">
            <br/>
            <em>For topic related records go to the reviewed book(s).</em>
            <br/>
            <br/>
        </xsl:if>
        <h5>Classification</h5>
        <ul>
            <xsl:value-of select="/mods:mods/mods:classification[@authority='spw-combined' or @authority='neu-combined']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:classification[@authority='spw-combined'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no classifications.</em></div></xsl:if>
        </ul>
        <h5>Time Periods</h5>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Time']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Time'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no links in this category.</em></div></xsl:if>
        </ul>
        <h5>Topics</h5>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Topic']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Topic'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no links in this category.</em></div></xsl:if>
        </ul>
        <h5>Places</h5>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Place']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Place'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no links in this category.</em></div></xsl:if>
        </ul>
        <h5>Persons</h5>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Person']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Person'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no links in this category.</em></div></xsl:if>
        </ul>
        <h5>Institutions</h5>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Corporation']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Corporation'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no links in this category.</em></div></xsl:if>
        </ul>
        <h5>Creator(s)</h5>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:extension/eac:Relation[@xlink:role='Creator']" />
            <xsl:variable name="count" select="count(/mods:mods/mods:extension/eac:Relation[@xlink:role='Creator'])" />
            <xsl:if test="$count = 0"><div class=""><em>There are no links in this category.</em></div></xsl:if>
        </ul>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:extension/eac:Relation[@xlink:role='Creator']">
        <li>
            <xsl:variable name="link" select="@xlink:href" />
            <a href="{$link}"><xsl:value-of select="." /></a>
            <xsl:value-of select="@localType" />
        </li>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Time']">
        <xsl:variable name="link" select="../@xlink:href" />
        <li><a href="{$link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Topic']">
        <xsl:variable name="link" select="../@xlink:href" />
        <li><a href="{$link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Place']">
        <xsl:variable name="link" select="../@xlink:href" />
        <li><a href="{$link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Person']">
        <xsl:variable name="link" select="../@xlink:href" />
        <li><a href="{$link}"><xsl:value-of select="." /></a></li>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:extension/eac:Relation[@xlink:role='Is about']/eac:relationEntry[@localType='Corporation']">
        <xsl:variable name="link" select="../@xlink:href" />
        <li><a href="{$link}"><xsl:value-of select="." /></a></li>
    </xsl:template>

    <!-- Review related items -->
    <xsl:template name="reviews">
        <hr/>
        <h4>Reviews</h4>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:relatedItem[@displayLabel='Is reviewed by']" />
        </ul>
    </xsl:template>

    <!-- Reviewed book related items -->
    <xsl:template name="review-of">
        <hr/>
        <h4>Reviewed Book(s)</h4>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:relatedItem[@type='reviewOf']" />
        </ul>
    </xsl:template>

    <!-- Journal Article related items -->
    <xsl:template name="journal-article-related">
        <hr/>
        <h4>Reviewed Book(s)</h4>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:relatedItem" />
        </ul>
    </xsl:template>

    <!-- Periodical related items -->
    <xsl:template name="periodical-related">
        <hr/>
        <h4>Table of Contents</h4>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:relatedItem[@type='constituent']" />
        </ul>
    </xsl:template>


    <xsl:template match="/mods:mods/mods:relatedItem">
        <xsl:variable name="modsRecordType" select="/mods:mods/mods:genre" />
        <xsl:choose>
            <xsl:when test="$modsRecordType = 'Journal Article'" >
                <li>
                    <xsl:variable name="link" select="@xlink:href" />
                    <xsl:value-of select="@displayLabel" />
                    <a href="{$link}">
                        <xsl:value-of select="mods:titleInfo/mods:title" />
                    </a> 
                </li>
            </xsl:when>
            <xsl:when test="$modsRecordType = 'Periodical'" >
                <li>
                    <xsl:variable name="link" select="@xlink:href" />
                    <xsl:apply-templates select="mods:name/mods:displayForm" />
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="mods:originInfo/mods:dateCreated" />)
                    <xsl:text> </xsl:text>
                    <a href="{$link}">
                        <xsl:value-of select="normalize-space(mods:titleInfo/mods:title)" />
                    </a> 
                    <xsl:text>, </xsl:text>
                    <xsl:value-of select="mods:part/mods:extent[@unit='pages']/mods:list" />
                </li>
            </xsl:when>
            <xsl:otherwise>
                <li>
                    <xsl:variable name="link" select="@xlink:href" />
                    <xsl:value-of select="@displayLabel" />
                    <xsl:text> </xsl:text>
                    <xsl:apply-templates select="mods:name/mods:displayForm" />
                    <xsl:text> </xsl:text>
                    (<xsl:value-of select="mods:originInfo/mods:dateCreated" />)
                    <xsl:text> </xsl:text>
                    <a href="{$link}">
                        <xsl:value-of select="mods:titleInfo/mods:title" />
                    </a> 
                </li>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Book Chapters -->
    <xsl:template name="chapters">
        <hr/>
        <h4>Includes Chapters</h4>
        <ul>
            <xsl:apply-templates select="/mods:mods/mods:relatedItem[@displayLabel='Includes chapter']" />
        </ul>
    </xsl:template>
    <xsl:template match="/mods:mods/mods:relatedItem[@displayLabel='Includes chapter']">
        <li>
            <xsl:variable name="link" select="@xlink:href" />
            <xsl:apply-templates select="mods:name/mods:displayForm" />
            <xsl:text> </xsl:text>
            <a href="{$link}">
                <xsl:value-of select="normalize-space(mods:titleInfo/mods:title)" />
            </a>
            <xsl:text>, </xsl:text>
            <xsl:value-of select="mods:part/mods:extent[@unit='pages']/mods:list" />
        </li>
    </xsl:template>

    <!-- Notes -->
    <xsl:template name="note">
        <xsl:apply-templates select="/mods:mods/mods:note" />
    </xsl:template>
    <xsl:template match="/mods:mods/mods:note">
        <div class="col-sm-2 col-md-2 col-lg-2">
            <xsl:value-of select="@displayLabel" />
        </div>
        <div class="col-sm-10 col-md-10 col-lg-10">
            <xsl:value-of select="." />
        </div>
    </xsl:template>

    <xsl:template match="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='editor']">
        <xsl:value-of select="../../mods:displayForm" />
        <xsl:text>; </xsl:text>
    </xsl:template> 

    <xsl:template match="/mods:mods/mods:relatedItem/mods:name/mods:role/mods:roleTerm[text()='editor']">
        <xsl:value-of select="../../mods:displayForm" />
        <xsl:text>; </xsl:text>
    </xsl:template> 

    <xsl:template match="/mods:mods/mods:name/mods:role/mods:roleTerm[text()='author']">
        <xsl:value-of select="../../mods:displayForm" />
        <xsl:text>; </xsl:text>
    </xsl:template> 

    <xsl:template match="/mods:mods/mods:relatedItem/mods:name/mods:role/mods:roleTerm[text()='author']">
        <xsl:value-of select="../../mods:displayForm" />
        <xsl:text>; </xsl:text>
    </xsl:template> 

    <xsl:template match="mods:name/mods:displayForm">
        <xsl:value-of select="." />
        <xsl:text>; </xsl:text>
    </xsl:template> 

</xsl:stylesheet>
