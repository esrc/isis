<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:eac="http://eac.staatsbibliotek-berlin.de/schema/cpf.xsd"
    xmlns:eac-cpf="urn:isbn:1-931666-33-4"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    exclude-result-prefixes="mods eac eac-cpf xlink str"
    version="1.0">
    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes" />

    <xsl:include href="common.xsl" />
    <xsl:include href="mods-book.xsl" />
    <xsl:include href="mods-chapter.xsl" />
    <xsl:include href="mods-journal-article.xsl" />
    <xsl:include href="mods-periodical.xsl" />
    <xsl:include href="mods-review.xsl" />
    <xsl:include href="eac-person.xsl" />
    <xsl:include href="eac-default.xsl" />

    <xsl:template name="content">
        <xsl:variable name="modsRecordType" select="/mods:mods/mods:genre" />
        <xsl:if test="$modsRecordType != ''">
            <xsl:choose>
                <xsl:when test="$modsRecordType = 'Chapter'">
                    <xsl:call-template name="process-chapter" />
                </xsl:when>
                <xsl:when test="$modsRecordType = 'Journal Article'">
                    <xsl:call-template name="process-journal-article" />
                </xsl:when>
                <xsl:when test="$modsRecordType = 'Book'">
                    <xsl:call-template name="process-book" />
                </xsl:when>
                <xsl:when test="$modsRecordType = 'Review'">
                    <xsl:call-template name="process-review" />
                </xsl:when>
                <xsl:when test="$modsRecordType = 'Periodical'">
                    <xsl:call-template name="process-periodical" />
                </xsl:when>
            </xsl:choose>
        </xsl:if>
        
        <xsl:variable name="eacRecordType" select="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:identity/eac-cpf:entityType" />
        <xsl:if test="$eacRecordType">
            <xsl:choose>
                <xsl:when test="$eacRecordType = 'person'">
                    <xsl:call-template name="process-person" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="process-eac-default" />
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
