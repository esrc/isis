<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:date="http://exslt.org/dates-and-times"
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:eac="http://eac.staatsbibliotek-berlin.de/schema/cpf.xsd"
    xmlns:eac-cpf="urn:isbn:1-931666-33-4"
    extension-element-prefixes="date"
    exclude-result-prefixes="mods eac eac-cpf date"
    version="1.0">

    <xsl:template name="footer">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <hr/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-2">
                <xsl:text>Last modified </xsl:text>
            </div>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <xsl:variable name="modsLastModified" select="/eac-cpf:eac-cpf/eac-cpf:control/eac-cpf:maintenanceHistory/eac-cpf:maintenanceEvent/eac-cpf:eventDateTime[1]" />
                <xsl:variable name="eacLastModified" select="/mods:mods/mods:recordInfo/mods:recordCreationDate" />
                <xsl:choose>
                    <xsl:when test="$modsLastModified != ''">
                        <xsl:value-of select="$modsLastModified" />
                    </xsl:when>
                    <xsl:when test="$eacLastModified != ''">
                        <xsl:value-of select="$eacLastModified" />
                    </xsl:when>
                    <xsl:otherwise> - </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-2">
                IsisCB Identifier
            </div>
            <div class="col-sm-9 col-md-9 col-lg-10">
                <xsl:variable name="modsCBBIdentifier" select="/mods:mods/mods:identifier[@type='isis']" />
                <xsl:variable name="eacCBBIdentifier" select="/eac-cpf:eac-cpf/eac-cpf:control/eac-cpf:recordId != ''" />
                <xsl:choose>
                    <xsl:when test="$modsCBBIdentifier != ''">
                        <xsl:value-of select="$modsCBBIdentifier" />
                    </xsl:when>
                    <xsl:when test="$eacCBBIdentifier != ''">
                        <xsl:value-of select="$eacCBBIdentifier" />
                    </xsl:when>
                    <xsl:otherwise> - </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>
        <div class="row">
            <hr/>
            <div class="col-sm-9 col-md-10 col-lg-10">
                <h4>IsisCB History of Science Society</h4>
                This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>.
            </div>
            <div class="col-sm-3 col-md-2 col-lg-2">
                <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
                    <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" />
                </a>
            </div>
        </div>
    </xsl:template>

</xsl:stylesheet>
