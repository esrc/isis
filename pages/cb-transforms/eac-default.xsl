<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:eac="http://eac.staatsbibliotek-berlin.de/schema/cpf.xsd"
    xmlns:eac-cpf="urn:isbn:1-931666-33-4"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    exclude-result-prefixes="mods eac eac-cpf xlink str"
    version="1.0">

    <xsl:import href="common.xsl" />

    <xsl:template name="process-eac-default">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <span style="background-color: #F8DAE2; padding: 15px; display: inline-block; width: 100%; border-radius: 8px;">
                    <xsl:value-of select="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:identity/eac-cpf:entityType" />
                </span>
                <h3><xsl:value-of select="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:identity/eac-cpf:nameEntry/eac-cpf:part[1]" /></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <xsl:call-template name="resources-about" />
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-2 col-md-2 col-lg-2">
                Source File
            </div>
            <div class="col-sm-10 col-md-10 col-lg-10">
                <xsl:variable name="link" select="/eac-cpf:eac-cpf/eac-cpf:cpfDescription/eac-cpf:identity/eac-cpf:entityId" />
                <a href="${@link}"><xsl:value-of select="$link" /></a>
            </div>
        </div>

    </xsl:template>
</xsl:stylesheet>
