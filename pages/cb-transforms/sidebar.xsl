<?xml version="1.0" encoding="UTF-8"?>
<!--
    Generator transform for Isis Current Bibliography
    Author: Marco La Rosa
    Copyright 2015 eScholarship Research Centre, The University of Melbourne

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:eac="http://eac.staatsbibliotek-berlin.de/schema/cpf.xsd"
    xmlns:eac-cpf="urn:isbn:1-931666-33-4"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:str="http://exslt.org/strings"
    extension-element-prefixes="str"
    exclude-result-prefixes="mods eac eac-cpf xlink str"
    version="1.0">

    <xsl:template name="sidebar">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <img src="assets/images/logo.png" style="max-width: auto; max-height: 150px;"/>
           </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <a class="" href=""><span class="glyphicon glyphicon-arrow-left"></span></a>
                &#160;&#160;&#160;
                <a class="" href="/home"><span class="glyphicon glyphicon-home"></span></a>
                &#160;&#160;&#160;
                <a class="" href="/search"><span class="glyphicon glyphicon-search"></span></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <br/>
                <span class="text-center" style="display: block;">- find related records -</span>
                <xsl:call-template name="mods-relationships" />
            </div>
        </div>
</xsl:template>
</xsl:stylesheet>
