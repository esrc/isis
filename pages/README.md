# XSL Transformation Code #

## Layout ##

* cb-transforms contains all of the XSL transforms. Then entry point is page.xsl. 
* sample_xml: contains sample XML data files.
* output/assets: the JS and css
* process.sh: a runner to iterate over each file in sample_xml transforming it to HTML stored in output
* transform: a python script which uses lxml to transform a document given a specific transform.

## Basic usage ##

./transform -d {file to transform} -t {transform to use} > {path to output}

## XSL structure ##

The entry point to the transforms is page.xsl. This file create the HTML page and its overall layout calling other transforms to 
fill in the sections. The main decision tree is then contained in content.xsl. Depending on the file type being handled, a 
specific transform will be loaded.

Note that the architecture produced basically expects to find a transform per type. Whilst this results in some duplication
of code, it does separate the concerns rather nicely.

Perhaps the other main transform that needs to be identified is common.xsl. As it's name implies, it's the common code though
not all templates defined in there are used across all types.

Finally, the model used to hook the common code into the type specific templates is as follows:

* in the type specific template, a call is made using <xsl:call-template name=""></xsl:call-template>
* in common, that template is labelled as <xsl:template name=""></xsl:template>. These templates generally
call <xsl:apply-templates select=""></xsl:apply-templates> which will match a template as 
<xsl:template match=""></xsl:template>
