# README #

Isis Current Bibliography Source Code

## Layout ##

The repository consists of 3 largely independent components kept together for ease of management.

* pages
    * The code to produce the HTML versions of the source XML data using XSLT
* search
    * The search application. 
        * Install node and grunt.
        * In the top level: grunt serve
        * Fire up $(your host):9000 in your browser and start coding
* pageview
    * The legacy digitised data browser

### Who do I talk to? ###

* Marco La Rosa (m@lr.id.au)