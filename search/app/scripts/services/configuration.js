'use strict';

/**
 * @ngdoc service
 * @name Configuration
 * @description
 *  Configuration object for the app.
 */
angular.module('searchApp')
  .constant('Configuration', {
      production:               'https://solr.esrc.unimelb.edu.au',
      testing:                  'https://data.esrc.info/solr',
      loglevel:                 'DEBUG',
      deployment:               'production',
      allowedRouteParams:       [ 'q' ],
      site:                     'ISISrecords',
      searchType:               'keyword',
      keywordSearchOperator:    'AND',
      datasetStart:             '0001-01-01T00:00:00Z',
      datasetEnd:               '2014-12-31T23:59:59Z',

      searchFields: {
          '0': { 'fieldName': 'author_search',           'displayName': 'Authors',         'weight': '1' },
          '1': { 'fieldName': 'editor_search',           'displayName': 'Editors',         'weight': '1' },
          '2': { 'fieldName': 'contributor_search',      'displayName': 'Contributors',    'weight': '1' },
          '3': { 'fieldName': 'title_search',            'displayName': 'Title',           'weight': '1' },
          '4': { 'fieldName': 'publisher_search',        'displayName': 'Publisher',       'weight': '1' },
          '5': { 'fieldName': 'subject_topic_search',    'displayName': 'Subject: topic',  'weight': '1' },
          '6': { 'fieldName': 'subject_personal_search', 'displayName': 'Subject: person', 'weight': '1' }
      },
      searchWhat:  [ '0', '1', '2', '3', '4', '5', '6' ]

  });
