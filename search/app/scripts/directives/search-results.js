'use strict';

angular.module('searchApp')
  .directive('searchResults', [ '$rootScope', '$window', 'SolrService', function ($rootScope, $window, SolrService) {
    return {
      templateUrl: 'views/search-results.html',
      restrict: 'E',
      scope: {
          'displayProvenance': '@'
      },
      link: function postLink(scope, element, attrs) {

          /* Initialise the widget / defaults */
          scope.showFilters = false;
          scope.site = SolrService.site;
          scope.summaryActive = '';
          scope.detailsActive = 'active';
          scope.page = 0;

          /* handle data updates */
          scope.$on('search-results-updated', function() {
              scope.results = SolrService.results;
              scope.filters = SolrService.getFilterObject();
              scope.togglePageControls();
          });

          scope.nextPage = function() {
              SolrService.nextPage();
          };
          scope.previousPage = function() {
              SolrService.previousPage();
          };

          scope.togglePageControls = function() {
              if (SolrService.start === 0) {
                  scope.disablePrevious = true;
              } else {
                  scope.disablePrevious = false;
              }

              if (SolrService.start + SolrService.rows >= scope.results.total) {
                  scope.disableNext = true;
              } else {
                  scope.disableNext = false;
              }
          }

          scope.clearAllFilters = function() {
            SolrService.clearAllFilters();
          };

      }
    };
  }]);
