'use strict';

/** 
 * @ngdoc directive
 * @name facet-widget
 * @restrict E
 * @scope 
 * @description
 *  A UI control for a SOLR facet. Displays the available content as a set
 *  of checkboxes that can be added to the query as filters.
 * 
 * @param {string} facetField - The field for which to get the facet counts
 * @param {string} label - The name to be used for the widget
 * @param {string} join - The operator for joining multiple selections (default: OR)
 *
 */
angular.module('searchApp')
  .directive('facetWidget', [ '$window', 'SolrService', 'Configuration', function ($window, SolrService, conf) {
    return {
        templateUrl: 'views/facet-widget.html',
        restrict: 'E',
        scope: {
            facetField: '@',
            label: '@',
            join: '@',
            isCollapsed: '@',
            alwaysOpen: '@',
            sortBy: '@'
        },
        link: function postLink(scope, element, attrs) {
            // configure defaults for those optional attributes if not defined
            scope.ao = scope.alwaysOpen === undefined                         ? false : angular.fromJson(scope.alwaysOpen);
            scope.ic = scope.isCollapsed === undefined                       ? true  : angular.fromJson(scope.isCollapsed);
            scope.sb = scope.sortBy === undefined ? 'count' : scope.sortBy;
            scope.smallList = true;
            scope.largeList = false;

            // facet offset and begining page size
            scope.offset = 0;
            scope.pageSize = 5;
            var updateFacetCounts = function() {
                SolrService.updateFacetCount(scope.facetField, scope.offset, scope.pageSize, scope.sb);
            }

            // when we get a bootstrap message - init the filter
            //scope.$on('app-ready', function() {
            //    updateFacetCounts();
            //})
            scope.$on('update-all-facets', function() {
                updateFacetCounts();
            })

            // set the union operator for multiple selections
            if (scope.join === undefined) { 
                scope.join = 'OR';
            }
            SolrService.filterUnion[scope.facetField] = scope.join;

            // when we get an update event for this widget from the solr
            //  service - rejig the widget as required
            scope.$on(scope.facetField+'-facets-updated', function() {
                var selected = SolrService.filters[scope.facetField];
                if (selected === undefined) { 
                    selected = []; 
                }

                var f = [];

                // iterate over the facet list as returned by solr and re - tick 
                //  anything that has been selected
                angular.forEach(SolrService.facets[scope.facetField], function(v,k) {
                    if (selected.indexOf(v[0]) !== -1) {
                        v[2] = true;
                        if (scope.startup === undefined) {
                            scope.ic = false;
                            scope.startup = false;
                        }
                    }

                    f.push(v);
                })

                if (f.length <= 5) {
                    scope.facets = f;
                } else {
                    scope.facets = [];
                    scope.facets.push(f.slice(0,15));
                    scope.facets.push(f.slice(15,30));
                    scope.facets.push(f.slice(30,45));
                    scope.facets.push(f.slice(45,60));
                }
            });

            // wipe clean if told to do so
            scope.$on('reset-all-filters', function() {
                angular.forEach(scope.facets, function(v,k) {
                    scope.facets[k][2] = false;
                })
                scope.selected = [];
            });

            // handle open / close broadcasts
            scope.$on('open-all-filters', function() {
                scope.ic = false;
            })
            scope.$on('close-all-filters', function() {
                scope.ic = true;
            })

            scope.reset = function() {
                scope.offset = 0;
                SolrService.clearFilter(scope.facetField);
                updateFacetCounts();
                angular.forEach(scope.facets, function(v,k) {
                    scope.facets[k][2] = false;
                })
                scope.selected = [];
            };
        
            scope.facet = function(facet) {
                SolrService.filterQuery(scope.facetField, facet);
            };

            scope.pageForward = function() {
                scope.offset = scope.offset + scope.pageSize;
                updateFacetCounts();
            }
            scope.pageBackward = function() {
                scope.offset = scope.offset - scope.pageSize;
                if (scope.offset < 0) { scope.offset = 0 };
                updateFacetCounts();
            }
            scope.showMore = function() {
                scope.smallList = false;
                scope.facets = [];
                scope.overlay = {
                    'position': 'relative',
                    'width': $window.innerWidth - 60,
                    'z-index': '20',
                    'background-color': 'white',
                    'border': '1px solid grey',
                    'border-radius': '8px',
                    'padding': '15px'
                }
                scope.underlay = {
                    'position': 'fixed',
                    'top': '0px',
                    'left': '0px',
                    'width': $window.innerWidth,
                    'height': $window.innerHeight,
                    'background-color': '#ccc',
                    'z-index': '10',
                    'opacity': 0.3,
                }
                scope.pageSize = 60;
                SolrService.updateFacetCount(scope.facetField, scope.offset, scope.pageSize, scope.sb);
                scope.largeList = true;
            }
            scope.close = function() {
                scope.largeList = false;
                scope.facets = [];
                scope.pageSize = 5;
                SolrService.updateFacetCount(scope.facetField, scope.offset, scope.pageSize, scope.sb);
                scope.smallList = true;
            }
      }
    };
  }]);
