'use strict';

angular.module('searchApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngAnimate',
  'ui.bootstrap'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        reloadOnSearch: false,
        resolve: { 
            'result': [ 'SolrService', function(SolrService) {
                var r = SolrService.init();
            }]
        }
      })
      .when('/small-device', {
          templateUrl: 'views/small-device.html'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
