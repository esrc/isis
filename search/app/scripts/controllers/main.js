'use strict';

angular.module('searchApp')
  .controller('MainCtrl', [ '$scope', '$window', '$location', '$timeout', 'SolrService', 'Configuration',
    function ($scope, $window, $location, $timeout, SolrService, conf) {
      $scope.authority = false;
      $scope.bibliographic = false;

      var w = angular.element($window);
      w.bind('resize', function() {
          $scope.$apply(function() {
            sizeThePanels();
          })
      });

      var sizeThePanels = function() {
          $scope.w = $window.innerWidth;
          $scope.h = $window.innerHeight;
          //console.log($scope.w, $scope.h);

          $scope.t = 100;
          if ($scope.w < 768) {
              $window.location.replace('#/small-device');
          } else if ($scope.w < 1024) {

              $scope.lpw = Math.floor(($scope.w) * 0.30) - 1;
              $scope.rpw = $scope.w - $scope.lpw - 1;
          } else {
              $scope.lpw = Math.floor(($scope.w) * 0.25) - 1;
              $scope.rpw = $scope.w - $scope.lpw - 1;
          }
          $scope.topbarStyle = {
              'position': 'fixed',
              'top':      '0px',
              'width':    '100%',
              'z-index':  '10000',
          }
          $scope.bodypanelStyle = {
              'position':         'absolute',
              'top':              $scope.t + 'px',
              'left':             '0px',
              'width':            '100%',
              'height':           '100%',
              'padding':          '5px 15px',
              'background-color': 'white'
          }
      }
      sizeThePanels();

      /* handle summary / detail view toggle */
      $scope.$on('show-search-results-details', function() {
          $scope.detailsActive = false;
      });
      $scope.$on('hide-search-results-details', function() {
          $scope.detailsActive = true;
      });
      $scope.$on('reset-all-filters', function() {
        if ($scope.authority) {
            $scope.setAuthority();
        } else {
            $scope.setBibliography();
        }
        SolrService.search();

      })
      $scope.$on('app-ready', function() {
          $scope.appReady = true;
          $scope.setBibliography(true);
          $scope.setSearchType(conf.searchType);
          SolrService.search();
      });
      $scope.$on('search-results-updated', function() {
          $scope.results = SolrService.results;
      });

      /* button methods */
      $scope.toggleDetails = function() {
          SolrService.toggleDetails();
      };

      // set up search for authority tab
      $scope.setAuthority = function(reset) {
          if ($scope.appReady !== true) { return; };
          $scope.bibliographic = false;
          SolrService.filters = {};
          SolrService.dateFilters = {};
          SolrService.filterQuery('record_type_short', 'EAC' );
          $scope.authority = true;
      }

      // set up search for bibliography tab
      $scope.setBibliography = function() {
          if ($scope.appReady !== true) { return; };
          $scope.authority = false;
          SolrService.filters = {};
          SolrService.dateFilters = {};
          SolrService.filterQuery('record_type_short', 'MODS' );
          $scope.bibliographic = true;
      }

      // keyword or phrase search ?
      $scope.setSearchType = function(type) {
          SolrService.searchType = type;
          if (SolrService.searchType === 'phrase') {
              $scope.keywordSearch = false;
              $scope.phraseSearch = true;
          } else {
              $scope.phraseSearch = false;
              $scope.keywordSearch = true;
              $scope.setSearchUnion(conf.keywordSearchOperator);
          }
          //scope.search();
      }

      // set the union for keyword search
      $scope.setSearchUnion = function(union) {
          SolrService.keywordUnion = union;
          if (SolrService.keywordUnion === 'AND') {
              $scope.keywordAnd = true;
              $scope.keywordOr = false;
          } else {
              $scope.keywordAnd = false;
              $scope.keywordOr = true;
          }
      }

      // toggle what get's searched
      //  if what is in the list - remove it
      //  if not in list - add it
      $scope.updateSearchLimit = function(what) {
          if ($scope.searchWhat[what] === true) {
              $scope.searchWhat[what] = false;
              SolrService.searchWhat.splice(SolrService.searchWhat.indexOf(what), 1);
          } else {
              $scope.searchWhat[what] = true;
              SolrService.searchWhat.push(what);
          }
          if ($scope.searchWhat.length != conf.searchWhat.length) {
              $scope.allSelectedToggle = true;
              $scope.noneSelectedToggle = true;
          } else {
              $scope.allSelectedToggle = false;
              $scope.noneSelectedToggle = true;
          }
      }

      // search all listed fields
      $scope.selectAll = function() {
          $scope.searchWhat = [];
          angular.forEach(conf.searchFields, function(v,k) {
              $scope.searchWhat.push(true);
          })
          SolrService.searchWhat = conf.searchWhat;
          $scope.allSelectedToggle = false;
          $scope.noneSelectedToggle = true;
      }

      // search none of the listed fields (this amounts
      //  to a blank search)
      $scope.deselectAll = function() {
          $scope.searchWhat = [];
          SolrService.searchWhat = [];
          $scope.allSelectedToggle = true;
          $scope.noneSelectedToggle = false;
      }

      // set the active tab
      $scope.tabs = [ true, false, false];

      // set up what is going to be searched
      //  on load - everything
      $scope.searchWhat = [];
      angular.forEach(conf.searchFields, function(v,k) {
          $scope.searchWhat.push(true);
      });
      $scope.allSelectedToggle = false;
      $scope.noneSelectedToggle = true;

  }]);
